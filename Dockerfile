FROM alpine:latest

RUN apk add --update --no-cache\
  mysql-client\
  openssh-client

ADD start.sh app/

RUN chmod +x /app/start.sh

EXPOSE 3306
ENTRYPOINT ["/app/start.sh"]
