#!/bin/sh

chown -R root:root /root/.ssh
chmod -R a-rwx,u+rwX /root/.ssh
chmod u-w /root/.ssh/id_ed25519

#ssh-keyscan -Ht ed25519 coptere.infra.ubuntu-fr.org > /root/.ssh/known_hosts
#ssh-keyscan -Ht ed25519 forum.infra.ubuntu-fr.org >> /root/.ssh/known_hosts

#ssh-keygen -R coptere.infra.ubuntu-fr.org
#ssh-keygen -R forum.infra.ubuntu-fr.org

#cat /root/.ssh/known_hosts

#ssh -o StrictHostKeyChecking=no -nNTvvv forum.infra -L 3306:10.42.1.10:3306
ssh -nNT forum.infra -L 3306:10.42.1.10:3306

#ssh -fNT forum.infra -L 3306:10.42.1.10:3306
