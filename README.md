# MySQL Tunnel

Service docker de tunnel SSH pour accéder à la db de l'ancien serveur depuis la nouvelle infrastructure.
type :
```
ssh forum.infra -L 3306:mysql-server:3306
```

## Utilisation

### Déchiffrer les variables et clés de connexion

- Installer [SOPS](https://github.com/getsops/sops/releases)
- Installer [age](https://github.com/FiloSottile/age) : `sudo apt install age`
- Placer la clé `ufr-key.txt` dans votre répertoire `~/.sops/`
- Ajouter en fin de fichier `~/.bashrc` la ligne
  ```
  export SOPS_AGE_KEY_FILE=$HOME/.sops/ufr-key.txt
  ```
- ```
  source ~/.bashrc
  ```
- Déchiffrer les fichiers :
  ```
  make decrypt
  ```
- Enregistrer les fichiers chiffrés après modification :
  ```
  make encrypt
  ```

([tuto fr](https://blog.stephane-robert.info/docs/securiser/secrets/sops/))

### Ajuster les permissions

#### production
```
make prod
```

#### développement
```
make dev
```

### Lancer le container
```
docker-compose up
```

### Connexion à la base de données
```
mysql -h127.0.0.1 -u$MYSQL_USER -p$MYSQL_PASSWORD
```