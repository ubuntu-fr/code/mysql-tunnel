help: ## Affiche ce message d'aide
	@printf "\n$(color_method)help\033[0m\n\n"
	@awk -F ':|##' '/^[^\t].+?:.*?##/ {\
		printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST) | sort
	@printf "\n"

decrypt: ## Déchiffre les secrets
	sops -d .pops.env > .env
	sops -d ssh/config.pops > ssh/config
	sops -d ssh/id_ed25519.pops > ssh/id_ed25519
	sops -d ssh/id_ed25519.pops.pub > ssh/id_ed25519.pub

encrypt: ## Enregistre les fichiers chiffrés après modification
	sops -e .env > .pops.env
	sops -e ssh/config > ssh/config.pops
	sops -e ssh/id_ed25519 > ssh/id_ed25519.pops
	sops -e ssh/id_ed25519.pub > ssh/id_ed25519.pops.pub

prod: ## Ajuste les permissions pour la production
	sudo chmod a-rwx,u+rX ssh ssh/id_ed25519
	sudo chown root:root -R ssh

dev: ## Ajuste les permissions pour le développement
	sudo chown $$USER:$$USER -R ssh
